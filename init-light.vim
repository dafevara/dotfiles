set nocompatible
syntax enable
syntax on
filetype plugin on
set nu
au BufNewFile,BufRead *.py
    \ set tabstop=4 |
    \ set softtabstop=4 |
    \ set shiftwidth=4 |
    \ set textwidth=80 |
    \ set expandtab |
    \ set autoindent |
    \ set wrap

au BufNewFile,BufRead *.ts
    \ set tabstop=2 |
    \ set softtabstop=2 |
    \ set shiftwidth=2 |
    \ set textwidth=80 |
    \ set expandtab |
    \ set autoindent

au BufNewFile,BufRead *.js
    \ set tabstop=2 |
    \ set softtabstop=2 |
    \ set shiftwidth=2 |
    \ set textwidth=80 |
    \ set expandtab |
    \ set autoindent

call plug#begin()
Plug 'sheerun/vim-polyglot'
Plug 'vim-ruby/vim-ruby'
Plug 'vim-python/python-syntax'
Plug 'tmux-plugins/vim-tmux-focus-events'
Plug 'mattn/msgpack-vim'
Plug 'chrisbra/csv.vim'
Plug 'tpope/vim-eunuch'
Plug 'lifepillar/vim-solarized8'
Plug 'vim-voom/VOoM'
Plug 'xolox/vim-misc'
Plug 'phanviet/vim-monokai-pro'
Plug 'skielbasa/vim-material-monokai'
Plug 'kadekillary/Turtles'
Plug 'chriskempson/base16-vim'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'ctrlpvim/ctrlp.vim'
"Plug 'scrooloose/nerdtree'
Plug 'rhysd/conflict-marker.vim'
Plug 'vim-scripts/sessionman.vim'
Plug 'godlygeek/tabular'
Plug 'majutsushi/tagbar'
Plug 'mbbill/undotree'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-fugitive'
Plug 'airblade/vim-gitgutter'
Plug 'tpope/vim-rails'
Plug 'tpope/vim-repeat'
Plug 'szw/vim-tags'
Plug 'tacahiroy/ctrlp-funky'
Plug 'FelikZ/ctrlp-py-matcher'
Plug 'altercation/vim-colors-solarized'
Plug 'flazz/vim-colorschemes'
Plug 'reedes/vim-pencil'
Plug 'dyng/ctrlsf.vim'
Plug 'mg979/vim-visual-multi'
Plug 'plasticboy/vim-markdown'

Plug 'leafgarland/typescript-vim'
Plug 'jason0x43/vim-js-indent'
Plug 'Quramy/vim-js-pretty-template'

Plug 'Quramy/tsuquyomi'
Plug 'prettier/vim-prettier'
Plug 'tmux-plugins/vim-tmux-focus-events'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
" JavaScript {{{
Plug 'othree/yajs.vim', { 'for': [ 'javascript', 'javascript.jsx', 'html', 'typescript', 'typescript.tsx', 'vue', 'javascript.vue' ] }
Plug 'pangloss/vim-javascript', { 'for': ['javascript', 'javascript.jsx', 'html', 'vue', 'javascript.vue'] }
Plug 'moll/vim-node', { 'for': ['javascript', 'typescript', 'typescript.tsx', 'vue', 'javascript.vue'] }
Plug 'mxw/vim-jsx', { 'for': ['javascript.jsx', 'javascript', 'typescript', 'typescript.tsx', 'vue', 'javascript.vue'] }
Plug 'peitalin/vim-jsx-typescript'
Plug 'ternjs/tern_for_vim', { 'for': ['javascript', 'javascript.jsx', 'typescript', 'typescript.tsx', 'vue', 'javascript.vue'], 'do': 'npm install' }
" }}}

" TypeScript {{{
Plug 'leafgarland/typescript-vim', { 'for': 'typescript' }
Plug 'Shougo/vimproc.vim', { 'do': 'make' }
Plug 'posva/vim-vue'

Plug 'mhartington/nvim-typescript', { 'for': 'typescript', 'do': './install.sh' }
let g:nvim_typescript#diagnosticsEnable = 0
let g:nvim_typescript#max_completion_detail=100
" }}}


" Styles {{{
" Plug 'wavded/vim-stylus', { 'for': ['stylus', 'markdown'] }
" Plug 'groenewege/vim-less', { 'for': 'less' }
" Plug 'hail2u/vim-css3-syntax', { 'for': 'css' }
" Plug 'cakebaker/scss-syntax.vim', { 'for': 'scss' }
" Plug 'gko/vim-coloresque'
" Plug 'stephenway/postcss.vim', { 'for': 'css' }

" Plug 'KeitaNakamura/neodark.vim'
Plug 'rakr/vim-one'
Plug 'joshdick/onedark.vim'
Plug 'EdenEast/nightfox.nvim' " Vim-Plug
Plug 'morhetz/gruvbox'
Plug 'NLKNguyen/papercolor-theme'
" }}}
"
" Completion {{{
if has('nvim')
  Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
else
  Plug 'Shougo/deoplete.nvim'
  Plug 'roxma/nvim-yarp'
  Plug 'roxma/vim-hug-neovim-rpc'
endif
Plug 'sickill/vim-pasta'
Plug 'roxma/nvim-yarp'
Plug 'roxma/vim-hug-neovim-rpc'
Plug 'tmux-plugins/vim-tmux-focus-events'
"Plug 'vim-scripts/dbext.vim'
"
" python
Plug 'Vimjas/vim-python-pep8-indent'
Plug 'python-mode/python-mode' ", { 'branch': 'develop' }
Plug 'vimwiki/vimwiki'
Plug 'lukas-reineke/indent-blankline.nvim'
Plug 'LoricAndre/OneTerm.nvim'
Plug 'ms-jpq/coq_nvim', {'branch': 'coq'}
Plug 'lifepillar/pgsql.vim'
Plug 'dense-analysis/ale'
Plug 'mfussenegger/nvim-lint'
Plug 'heavenshell/vim-pydocstring', { 'do': 'make install', 'for': 'python' }
call plug#end()
"""""""""""""""""""""o
""""""""""""""""""""""""""""""""


"let g:pymode_lint_unmodified = 1
" let g:pymode_debug = 1

let g:pymode_virtualenv = 1
let g:pymode_virtualenv_path = '/Users/dafevara/.pyenv/versions/3.8.5/envs/default'
let g:pymode_python = 'python3'
let g:python3_host_prog = '~/.pyenv/versions/3.8.5/envs/default/bin/python3'


"let g:pymode_doc = 1
let g:pymode_lint = 1
"let g:pymode_lint_on_fly = 1
"let g:pymode_lint_message = 1
let g:pymode_lint_checkers = ['black', 'flake8', 'pep8']
let g:pymode_lint_ignore = ["E501", "E128", "W605", "E231", "E303"]
let g:pymode_indent = 1

"""""""" DEOPLETE """"
let g:deoplete#enable_at_startup = 1

""""""""""""""""""""""""""""""""
""""" ALE """"""""""""""""""""""
let g:ale_linters_explicit = 1
let g:ale_python_vulture_options = '--min-confidence 65'
let g:ale_python_pycodestyle_options = '--max-line-length=82'
let g:ale_python_autopep8_options = '--max-line-length 82'
let g:ale_python_pep8_options = '--max-line-length 82'
let g:ale_linters = {
\   'python': ['autopep8', 'autoimport', 'black', 'pyflyby', 'mypy', 'flakehell', 'vulture', 'pep8', 'pycodestyle'],
\}
let g:ale_echo_msg_error_str = 'E'
let g:ale_echo_msg_warning_str = 'W'
let g:ale_echo_msg_format = '[%linter%] %s [%severity%]'
""""""""""""""""""""""
""""""""""""""""""""""
" BASICS
" set ttyfast
" set clipboard=unnamed
" set wildmode=list:longest
" set foldmethod=syntax " fold based on indent
" set foldlevelstart=99
" set foldnestmax=10 " deepest fold is 10 levels
" set nofoldenable " don't fold by default
" set foldlevel=1
" " set guicursor=n-v-c:block,i-ci-ve:ver25,r-cr:hor20,o:hor50
" " 			\,a:blinkwait700-blinkoff400-blinkon250-Cursor/lCursor
" " 			\,sm:block-blinkwait175-blinkoff150-blinkon175

filetype plugin on
set encoding=utf8
let g:pencil#textwidth = 80
let mapleader=','
set nu
set laststatus=2
let t_Co=256
set tags=./tags

set mat=2
" if &term =~ 'color'
" 	" disable background color erase
" 	set t_ut=
" endif

set wrap
set backspace=indent,eol,start
set exrc
set secure
if has('mouse')
	set mouse=a
endif
"set ruler
"set cursorline
"set cursorcolumn

"set shiftwidth=2
"set softtabstop=2
"set tabstop=2

"# for html/rb files, 2 spaces
autocmd Filetype html setlocal ts=2 sw=2 expandtab
autocmd Filetype ruby setlocal ts=2 sw=2 expandtab
autocmd Filetype python setlocal ts=4 sw=4 expandtab wrap
autocmd Filetype javascript setlocal ts=2 sw=2 expandtab

set splitbelow
set splitright
set undofile

" Standard “free” key where you can place custom mappings under https://nvie.com/posts/how-i-boosted-my-vim/
let mapleader = ","

"let g:deoplete#enable_at_startup = 1
" Make backspace behave in a sane manner.
set backspace=indent,eol,start

if has("autocmd")
	filetype indent plugin on
endif
let g:vim_tags_use_language_field = 1

autocmd BufNewFile,BufReadPost *.md set filetype=markdown

autocmd BufWritePre * :%s/\s\+$//e
set wildignore+=*/tmp/*,*.so,*.swp,*.zip     " MacOSX/Linux"

"""""""""""""""""""""""
" LOOK AND FEEL

" if (empty($TMUX))
"   if (has("nvim"))
"     "For Neovim 0.1.3 and 0.1.4 < https://github.com/neovim/neovim/pull/2198 >
"     let $NVIM_TUI_ENABLE_TRUE_COLOR=1
"   endif
"   "For Neovim > 0.1.5 and Vim > patch 7.4.1799 < https://github.com/vim/vim/commit/61be73bb0f965a895bfb064ea3e55476ac175162 >
"   "Based on Vim patch 7.4.1770 (`guicolors` option) < https://github.com/vim/vim/commit/8a633e3427b47286869aa4b96f2bfc1fe65b25cd >
"   " < https://github.com/neovim/neovim/wiki/Following-HEAD#20160511 >
"   if (has("termguicolors"))
"     set termguicolors
"   endif
" endif

"let $NVIM_TUI_ENABLE_TRUE_COLOR=1
set termguicolors

let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
" let g:solarized_termcolors=256
let g:solarized_degrade=0
let g:solarized_termtrans=0
let g:solarized_visibility='normal'
let g:solarized_contrast='high'
let g:solarized_diffmode='normal'
let g:solarized_extra_hi_groups=1
let g:solarized_statusline=1

let g:solarized_underline=1
let g:solarized_italic=1
let g:solarized_bold=1
let g:solarized_term_italics=1

let g:hybrid_custom_term_colors=1
let g:hybrid_reduced_contrast=0

let g:onedark_terminal_italics=1
let g:onedark_terminal_bolds=1
let g:onedark_hide_endofbuffer=1

let g:one_allow_italics=1 " I love italic for comments

let g:onedark_termcolors=256
"
" let base16colorspace=256
" "

"colorscheme solarized8_dark_high
set background=light        " for the light version
colorscheme one




" " one dark config
" let g:onedark_color_overrides = {"black": {"gui": "#1a1d21", "cterm": "7", "cterm16": "7" }}
"

"""""""""""""""""""""""""""""
" PLUGIN CONFIG

" airline
let g:airline#extensions#ale#enabled = 1
let g:airline_powerline_fonts = 1
"let g:airline_theme='onedark'
let g:airline_theme='light'
let g:airline#extensions#tabline#formatter = 'unique_tail_improved'

" nerdtrd
" let g:nerdtree_tabs_open_on_gui_startup=0
" let NERDTreeKeepTreeInNewTab=1
" let NERDTreequitOnOpen=0
" let NERDTreeShowBookmarks=0
" let NERDTreeChDirMode=0
"
" let g:NERDTreeHijackNetrw = 1
" au VimEnter NERD_tree_1 enew | execute 'NERDTree '.argv()[0]
" let NERDTreeIgnore=['\.pyc$', '\~$'] "ignore files in NERDTree"

" CTRLP
let g:ctrlp_custom_ignore = {'dir':  '\v[\/]\.(git|hg|svn)$'}
let g:ctrlp_user_command = ['.git/', 'git --git-dir=%s/.git ls-files -oc --exclude-standard']
let g:ctrlp_match_func = { 'match': 'pymatcher#PyMatch'  }
let g:ctrlp_map='<c-p>'
let g:ctrlp_cmd = 'CtrlPMRU'

" Git gutter
let g:gitgutter_realtime = 1
let g:gitgutter_eager = 0

" Voom
let g:voom_python_versions = [3]

" Pencil
let g:pencil#textwidth = 120

augroup pencil
	autocmd!
	autocmd FileType md,markdown,mkd call pencil#init({'wrap': 'soft'})
	autocmd FileType text         call pencil#init({'wrap': 'soft'})
augroup END

" markdown
autocmd BufNewFile,BufReadPost *.md set filetype=markdown
let g:markdown_fenced_languages = ['html', 'python', 'bash=sh', 'ruby']
let g:vim_markdown_folding_disabled = 0
let g:vim_markdown_override_foldtext = 0
set conceallevel=2
let g:vim_markdown_new_list_item_indent = 2

" CtrlSF
let g:ctrlsf_auto_focus = {
			\ "at": "start",
			\ "duration_less_than": 10
			\ }

let g:ctrlsf_case_sensitive = 'yes'
let g:ctrlsf_default_root = 'cwd'
let g:ctrlsf_winsize = '40%'

" Undotree
if has("persistent_undo")
	set undodir=~/.vimundo/
	set undofile
endif


" MAPPING
" Faster pane jump https://robots.thoughtbot.com/vim-splits-move-faster-and-more-naturally
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

nnoremap <C-F> :CtrlSF<Space>
"map <C-\> :NERDTreeToggle<CR>
map <C-\> :CocCommand explorer<CR>
map <C-K> :CtrlP<CR>
map <C-L> :CtrlPBuffer<CR>

"let loaded_matchparen=1 " Don't load matchit.vim (paren/bracket matching)
" set showmatch         " Don't match parentheses/brackets
" set lazyredraw          " Wait to redraw
" set scrolljump=8        " Scroll 8 lines at a time at bottom/top
" let html_no_rendering=1 " Don't render italic, bold, links in HTML
" let opacity=100
" let blur=0

au BufRead /tmp/psql.edit.* set syntax=sql

noremap <silent> <Cmd-S>          :update<CR>
vnoremap <silent> <Cmd-S>         <C-C>:update<CR>
inoremap <silent> <Cmd-S>         <C-O>:update<CR>

"noremap <silent> <Cmd-Z>          :undo<CR>
"vnoremap <silent> <Cmd-Z>         <Cmd-C>:undo<CR>
"inoremap <silent> <Cmd-Z>         <Cmd-O>:undo<CR>

"vmap <Cmd-c> "+yi
"vmap <Cmd-x> "+c
"vmap <Cmd-v> c<ESC>"+p
"imap <Cmd-v> <Cmd-r><Cmd-o>+
"
"Copy to 'clipboard registry'
vmap <Cmd-c> "*y
"
"Select all text
nmap <Cmd-a> ggVG

"normal copy/paste
vmap <Cmd-c> y<Esc>i
vmap <Cmd-x> d<Esc>i
imap <Cmd-v> <Esc>pi
"imap <Cmd-y> <Esc>ddi
map <Cmd-z> <Esc>
imap <Cmd-z> <Esc>ui
"let g:indentLine_char = '▏'



highlight GitGutterAdd    guifg=#003c4a ctermfg=2 ctermbg=0
highlight GitGutterChange guifg=#bbbb00 ctermfg=3 ctermbg=0
highlight GitGutterDelete guifg=#002222 ctermfg=5 ctermbg=0
highlight SignColumn ctermbg=0

" highlight GitGutterAdd    guifg=#003c4a ctermfg=2 ctermbg=7
" highlight GitGutterChange guifg=#bbbb00 ctermfg=3 ctermbg=7
" highlight GitGutterDelete guifg=#002222 ctermfg=5 ctermbg=7
" highlight SignColumn ctermbg=7

let g:python_highlight_all = 1
"let python_highlight_all = 1
" let python_self_cls_highlight = 1
" let python_no_operator_highlight = 0
" let python_no_parameter_highlight = 1
" let python_space_error_highlight = 1


" set t_8b=[48;2;%lu;%lu;%lum
" set t_8f=[38;2;%lu;%lu;%lum

" CUSTOM METHODS
function! ToggleApparenceMode()
    if &background != 'dark'
        colorscheme solarized
        set background=dark
        set termguicolors!
        let g:airline_theme='powerlineish'
        call airline#themes#powerlineish#refresh()

        " highlight GitGutterAdd    guifg=#003c4a ctermfg=2 ctermbg=0
        " highlight GitGutterChange guifg=#bbbb00 ctermfg=3 ctermbg=0
        " highlight GitGutterDelete guifg=#002222 ctermfg=5 ctermbg=0
        " highlight SignColumn ctermbg=0

        " colorscheme one
        " set background=dark
        " call one#highlight('Normal', '9da2ad', '14161a', 'none')
        " call one#highlight('CursorLine', 'none', '1b1e22', 'none')
        " set termguicolors
        " let g:airline_theme='onedark'
        " call airline#themes#one#refresh()
    else
        set background=light
        colorscheme one
        set termguicolors
        let g:airline_theme='light'
        call airline#themes#one#refresh()
    endif
endfunction

map <leader>m :call ToggleApparenceMode()<CR>
"imap <C-m> <Esc>:call ToggleApparenceMode()<CR>
"vmap <C-m> <Esc>:call ToggleApparenceMode()<CR>
"
"
"
"
"""""""""""""""""""""""""""""""""""""""""""""""""""
" COC CONFIG
" Set internal encoding of vim, not needed on neovim, since coc.nvim using some
" unicode characters in the file autoload/float.vim
set encoding=utf-8

" TextEdit might fail if hidden is not set.
set hidden

" Some servers have issues with backup files, see #649.
set nobackup
set nowritebackup

" Give more space for displaying messages.

" Having longer updatetime (default is 4000 ms = 4 s) leads to noticeable
" delays and poor user experience.
set updatetime=300

" Don't pass messages to |ins-completion-menu|.
set shortmess+=c

" Always show the signcolumn, otherwise it would shift the text each time
" diagnostics appear/become resolved.
" if has("nvim-0.5.0") || has("patch-8.1.1564")
"   " Recently vim can merge signcolumn and number column into one
"   set signcolumn=number
" else
"   set signcolumn=yes
" endif

set rtp+=/opt/homebrew/bin/fzf

let g:ackprg = 'ag --vimgrep'


let g:vimwiki_list = [{'path': '~/vimwiki/',
                      \ 'syntax': 'markdown', 'ext': '.md'}]
let g:vimwiki_global_ext = 0
let g:vimwiki_autowriteall = 1


"   " Use tab for trigger completion with characters ahead and navigate.
"   " NOTE: There's always complete item selected by default, you may want to enable
"   " no select by `"suggest.noselect": true` in your configuration file.
"   " NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
"   " other plugin before putting this into your config.
"   inoremap <silent><expr> <TAB>
"         \ coc#pum#visible() ? coc#pum#next(1) :
"         \ CheckBackspace() ? "\<Tab>" :
"         \ coc#refresh()
"   inoremap <expr><S-TAB> coc#pum#visible() ? coc#pum#prev(1) : "\<C-h>"
"
"   " Make <CR> to accept selected completion item or notify coc.nvim to format
"   " <C-g>u breaks current undo, please make your own choice.
"   inoremap <silent><expr> <CR> coc#pum#visible() ? coc#pum#confirm()
"                                 \: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"
"
"   function! CheckBackspace() abort
"     let col = col('.') - 1
"     return !col || getline('.')[col - 1]  =~# '\s'
"   endfunction
"
"   " Use <c-space> to trigger completion.
"   if has('nvim')
"     inoremap <silent><expr> <c-space> coc#refresh()
"   else
"     inoremap <silent><expr> <c-@> coc#refresh()
"   endif
