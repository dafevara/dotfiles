
syntax on
au BufRead /tmp/psql.edit.* set syntax=sql
syntax enable
set go-=L
set go-=r
set go-=T

autocmd FileType * set tabstop=4|set shiftwidth=4|set noexpandtab
au FileType python set tabstop=4
au FileType python set shiftwidth=4
au FileType python set expandtab
au FileType python set autoindent
au FileType python set smartindent
au FileType python set textwidth=80
au Filetype python setl et ts=4 sw=4

au InsertLeave * hi Cursor guibg=red
au InsertEnter * hi Cursor guibg=green

set guifont=D2coding\ for\ Powerline:h14

call plug#begin()
Plug 'chrisbra/csv.vim'
Plug 'tpope/vim-eunuch'
Plug 'lifepillar/vim-solarized8'
Plug 'szw/vim-tags'
Plug 'vim-voom/VOoM'
Plug 'xolox/vim-misc'
Plug 'xolox/vim-notes'
Plug 'phanviet/vim-monokai-pro'
Plug 'skielbasa/vim-material-monokai'
Plug 'kadekillary/Turtles'
Plug 'chriskempson/base16-vim'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'scrooloose/nerdtree'
Plug 'rhysd/conflict-marker.vim'
Plug 'vim-scripts/sessionman.vim'
Plug 'godlygeek/tabular'
Plug 'majutsushi/tagbar'
Plug 'mbbill/undotree'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-fugitive'
Plug 'nvie/vim-flake8'
Plug 'airblade/vim-gitgutter'
Plug 'tpope/vim-rails'
Plug 'tpope/vim-repeat'
Plug 'szw/vim-tags'
Plug 'tacahiroy/ctrlp-funky'
Plug 'FelikZ/ctrlp-py-matcher'
Plug 'altercation/vim-colors-solarized'
Plug 'flazz/vim-colorschemes'
Plug 'reedes/vim-pencil'
Plug 'dyng/ctrlsf.vim'
Plug 'terryma/vim-multiple-cursors'
Plug 'plasticboy/vim-markdown'
Plug 'leafgarland/typescript-vim'
Plug 'kchmck/vim-coffee-script'
Plug 'jason0x43/vim-js-indent'
Plug 'Quramy/vim-js-pretty-template'
Plug 'Quramy/tsuquyomi'
Plug 'prettier/vim-prettier'
Plug 'tmux-plugins/vim-tmux-focus-events'
" JavaScript {{{
Plug 'othree/yajs.vim', { 'for': [ 'javascript', 'javascript.jsx', 'html', 'typescript', 'typescript.tsx' ] }
Plug 'pangloss/vim-javascript', { 'for': ['javascript', 'javascript.jsx', 'html'] }
Plug 'moll/vim-node', { 'for': ['javascript', 'typescript', 'typescript.tsx'] }
Plug 'mxw/vim-jsx', { 'for': ['javascript.jsx', 'javascript', 'typescript', 'typescript.tsx'] }
Plug 'peitalin/vim-jsx-typescript'
Plug 'ternjs/tern_for_vim', { 'for': ['javascript', 'javascript.jsx', 'typescript', 'typescript.tsx'], 'do': 'npm install' }
" }}}

" TypeScript {{{
Plug 'leafgarland/typescript-vim', { 'for': 'typescript' }
Plug 'Shougo/vimproc.vim', { 'do': 'make' }

" }}}


" Styles {{{
Plug 'wavded/vim-stylus', { 'for': ['stylus', 'markdown'] }
Plug 'groenewege/vim-less', { 'for': 'less' }
Plug 'hail2u/vim-css3-syntax', { 'for': 'css' }
Plug 'cakebaker/scss-syntax.vim', { 'for': 'scss' }
Plug 'gko/vim-coloresque'
Plug 'stephenway/postcss.vim', { 'for': 'css' }

Plug 'rakr/vim-one'
Plug 'joshdick/onedark.vim'

" }}}
" Completion {{{
" if has('nvim')
"   Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
" else
"   Plug 'Shougo/deoplete.nvim'
"   Plug 'roxma/nvim-yarp'
"   Plug 'roxma/vim-hug-neovim-rpc'
" endif

"let g:deoplete#enable_at_startup = 1
" }}}
Plug 'sickill/vim-pasta'
Plug 'tmux-plugins/vim-tmux-focus-events'
"Plug 'vim-scripts/dbext.vim'
"
" python
Plug 'python-mode/python-mode', { 'branch': 'develop' }
call plug#end()

let g:deoplete#enable_at_startup = 1
"""""""""""""""""""
" BASICS
set ttyfast
set clipboard=unnamed
set wildmode=list:longest
set foldmethod=syntax " fold based on indent
set foldlevelstart=99
set foldnestmax=10 " deepest fold is 10 levels
set nofoldenable " don't fold by default
set foldlevel=1
set guicursor=n-v-c:block,i-ci-ve:ver25,r-cr:hor20,o:hor50
			\,a:blinkwait700-blinkoff400-blinkon250-Cursor/lCursor

set backupdir=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp
set directory=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp
set clipboard=unnamed
filetype plugin on
set encoding=utf8
let g:pencil#textwidth = 80
let mapleader=','
set nu
set laststatus=2
set nocompatible
let t_Co = 256
if (&term =~? 'mlterm\|xterm\|xterm-256\|screen-256')
        set ttymouse=xterm2
end

if &term =~ '256color'
  " disable background color erase
  set t_ut=
endif

if has('mouse')
	set mouse=a
endif


" Standard “free” key where you can place custom mappings under https://nvie.com/posts/how-i-boosted-my-vim/
let mapleader = ","

" Make backspace behave in a sane manner.
set backspace=indent,eol,start
syntax on
syntax enable
if has("autocmd")
        filetype indent plugin on
endif
let g:vim_tags_use_language_field = 1

autocmd BufNewFile,BufReadPost *.md set filetype=markdown
autocmd BufWritePre * :%s/\s\+$//e
set wildignore+=*/tmp/*,*.so,*.swp,*.zip     " MacOSX/Linux"

if (has("termguicolors"))
  " set termguicolors
endif
"""""""""""""""""""""""
" LOOK AND FEEL

let g:solarized_termolors=256
let g:solarized_degrade = 0
let g:solarized_termtrans = 0
let g:solarized_visibility = 'normal'
let g:solarized_contrast = 'high'
let g:solarized_diffmode = 'normal'

let g:solarized_underline = 1
let g:solarized_italic = 1
let g:solarized_bold = 1
let g:solarized_term_italics=1

let g:hybrid_custom_term_colors = 1
let g:hybrid_reduced_contrast = 0
let g:onedark_termcolors=1
let g:onedark_terminal_italics = 1
let g:onedark_hide_endofbuffer = 1

let g:onedark_termcolors=256


" one dark config
let g:onedark_color_overrides = {
\ "black": {"gui": "171a1f", "cterm": "235", "cterm16": "0" },
\}

"colorscheme solarized8_dark
"colorscheme base16-onedark
colorscheme solarized

set linespace=2
"""""""""""""""""""""""""""""

"""""""""""""""""""""""""""""
" PLUGIN CONFIG

" airline
let g:airline_powerline_fonts = 1
let g:airline_theme='hybridline'
let g:airline#extensions#tabline#formatter = 'unique_tail_improved'

" nerdtrd
let g:nerdtree_tabs_open_on_gui_startup=0
let NERDTreeKeepTreeInNewTab=1
let NERDTreequitOnOpen=0
let NERDTreeShowBookmarks=0
let NERDTreeChDirMode=0

let g:NERDTreeHijackNetrw = 1
let NERDTreeIgnore=['\.pyc$', '\~$'] "ignore files in NERDTree"

" CTRLP
let g:ctrlp_custom_ignore = {'dir':  '\v[\/]\.(git|hg|svn)$'}
let g:ctrlp_user_command = ['.git/', 'git --git-dir=%s/.git ls-files -oc --exclude-standard']
let g:ctrlp_match_func = { 'match': 'pymatcher#PyMatch'  }
let g:ctrlp_map='<c-p>'
let g:ctrlp_cmd = 'CtrlPMRU'

" Git gutter
let g:gitgutter_realtime = 0
let g:gitgutter_eager = 1



let g:pymode_python = 'python'
let g:pymode_doc = 0
let g:pymode_lint_on_fly = 0
let g:pymode_lint_message = 1
let g:pymode_lint_checkers = ['pyflakes', 'pep8', 'mccabe']
let g:pymode_lint_ignore = ["E501", "E128"]
" Notes

let g:notes_directories = ['~/Google Drive/NOTES']
let g:notes_suffix = '.md'
let g:notes_title_sync = 'rename_file'
let g:notes_unicode_enabled = 1
let g:notes_smart_quotes = 1
let g:notes_conceal_code = 1
let g:notes_conceal_italic = 1
let g:notes_conceal_bold = 1
let g:notes_conceal_url = 1
let g:notes_ruler_text = 1
let g:notes_tab_indents = 1

" Voom
let g:voom_python_versions = [3]

" Pencil
let g:pencil#textwidth = 120

augroup pencil
  autocmd!
  autocmd FileType md,markdown,mkd call pencil#init({'wrap': 'soft'})
  autocmd FileType text         call pencil#init({'wrap': 'soft'})
augroup END

" markdown
autocmd BufNewFile,BufReadPost *.md set filetype=markdown
let g:markdown_fenced_languages = ['html', 'python', 'bash=sh', 'ruby']
let g:vim_markdown_folding_disabled = 0
let g:vim_markdown_override_foldtext = 0
set conceallevel=2
let g:vim_markdown_new_list_item_indent = 2

" CtrlSF
let g:ctrlsf_auto_focus = {
    \ "at": "start",
    \ "duration_less_than": 10
    \ }

let g:ctrlsf_case_sensitive = 'no'
let g:ctrlsf_default_root = 'cwd'
let g:ctrlsf_winsize = '40%'

" Undotree
if has("persistent_undo")
  set undodir=~/.vimundo/
  set undofile
endif

" MAPPING
" Faster pane jump https://robots.thoughtbot.com/vim-splits-move-faster-and-more-naturally
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

nnoremap <C-F> :CtrlSF<Space>

map <C-\> :NERDTreeToggle<CR>
map <C-K> :CtrlP<CR>
map <C-L> :CtrlPBuffer<CR>
" set filetypes as typescript.tsx
autocmd BufNewFile,BufRead *.tsx,*.jsx set filetype=typescript.tsx
" dark red
hi tsxTagName ctermfg=darkblue cterm=bold

" darkyellow
hi tsxCloseString ctermfg=darkblue
hi tsxCloseTag ctermfg=darkblue cterm=bold
hi tsxAttributeBraces ctermfg=darkred
hi tsxEqual ctermfg=darkred

" yellow
hi tsxAttrib ctermfg=darkcyan
" light-grey
hi tsxTypeBraces ctermfg=grey
" dark-grey
hi tsxTypes ctermfg=darkgreen
hi Events ctermfg=204 guifg=#56B6C2
hi ReduxKeywords ctermfg=204 guifg=#C678DD
hi WebBrowser ctermfg=204 guifg=#56B6C2
hi ReactLifeCycleMethods ctermfg=204 guifg=#D19A66
